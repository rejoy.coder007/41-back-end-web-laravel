<!DOCTYPE html>
<html lang=en>

<head>


    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <meta http-equiv=ScreenOrientation content=autoRotate:disabled>
    <meta name=viewport content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
    <meta name=description content="">
    <meta name=author content="">

    <script defer  type="text/javascript" src="01_SCRIPTS/ab_shop_full.js"></script>
    <link rel="shortcut icon" type=image/png href="02_IMAGES/favicon.png">
    <title>@yield('title')</title>


    @yield('cssBlockExtra')
    <style>  @include('01_CSS.ab_shop_full') </style>

</head>

<body>

@yield('content')


</body>

</html>