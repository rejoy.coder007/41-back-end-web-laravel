<?php

namespace App\Http\Controllers;

use App\aa_ProductWeb;
use Illuminate\Http\Request;

class ab_ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {


        $random_products = aa_ProductWeb::all();

        return view("AB_SHOP_FULL.main")->with('random_products',$random_products);

    }



    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        //


        $selected_product = aa_ProductWeb::where('slug',$slug)->firstorFail();

        $mightAlsoLikes = aa_ProductWeb::where('slug', '!=', $slug)->inRandomOrder()->take(4)->get();
      //  dd($product);

       // return view("AC_PRODUCT_DETAILS.main")->with('selected_product',$selected_product);


        return view("AC_PRODUCT_DETAILS.main")->with([
            'selected_product' => $selected_product,
            'mightAlsoLikes' => $mightAlsoLikes,

        ]);

    }

}
