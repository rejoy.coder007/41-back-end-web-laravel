<?php
use App\aa_ProductWeb;
Route::get("/aa_home","AA_HOME\main@_aa_home");


///// Final Design /////////
Route::get("/","aa_HomePageController@index")->name("home-page");
Route::get("/shop-full","ab_ShopController@index")->name('shop.full');
Route::get("/shop-full/{slug}","ab_ShopController@show")->name('shop.show.item');
Route::get('/cart-details', 'za_Cart\CartMangementController@index')->name('cart.index');
Route::post('/cart-details', 'za_Cart\CartMangementController@store')->name('cart.store');
Route::post('/cart/cart-save-later/{product}', 'za_Cart\CartMangementController@cart_save_later')->name('cart.saveLater');
Route::delete('/saveForLater/{product}', 'za_Cart\SaveForLaterController@destroy')->name('saveForLater.destroy');
Route::post('/saveForLater/switchToCart/{product}', 'za_Cart\SaveForLaterController@switchToCart')->name('saveForLater.switchToCart');
Route::delete('/cart-details/{product}', 'za_Cart\CartMangementController@destroy')->name('cart.destroy');
Route::get('/checkout', 'za_Cart\checkOutController@index')->name('checkout.index');



//Route::get("/shop-full","ab_ShopController@index")->name("home-page");
Route::get("/product-details","AC_PRODUCT_DETAILS\main@_ac_product_details");
//Route::get("/cart-details","AD_CART\main@_ad_cart");



Route::get("/checkout-product","AE_CHECKOUT\main@_ae_checkout");

Route::get("/thankyou-for-purchase","AF_THANKYOU\main@_af_thankyou");
Route::get('/empty', function () {
    echo 'Hello World';
    \Gloudemans\Shoppingcart\Facades\Cart::destroy();
    Cart::instance('saveForLater')->destroy();

});




/////// Test ////////////////////////////////////////////////////
Route::get("/full_products","ab_ShopController@index");

Route::get("/{slug}","ab_ShopController@show");



//
//



