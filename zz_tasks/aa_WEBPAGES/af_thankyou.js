var SCRIPTS_OUT_PATH                                                = 'aa_template/public/01_SCRIPTS';
var SCRIPTS_IN_PATH                                                = '02_SCRIPTS/AF_THANKYOU/**/*.*';
var SCRIPTS_OUT_FILE_NAME                                                          = 'af_thankyou.js';


var SASS_OUT_PATH                                                       = 'aa_template/resources/views/01_CSS';
var SASS_IN_PATH                                      = '01_SASS/AF_THANKYOU/style.scss';
var SASS_OUT_FILE_NAME                                                 = 'af_thankyou.blade.php';

var VIEW_T_IN_FRAG                         = '03_ViewsT/ab_Fragments/AF_THANKYOU/**/*.*';
var VIEW_T_OUT_BODY                      = '03_ViewsT/aa_WorkSpace/zz_body/AF_THANKYOU';
 

module.exports = 
{
  
  af_thankyou_mode : function ()
  {
  
                var options_script = {
                    SCRIPTS_OUT_PATH: SCRIPTS_OUT_PATH,
                    SCRIPTS_IN_PATH: SCRIPTS_IN_PATH,
                    SCRIPTS_OUT_FILE_NAME: SCRIPTS_OUT_FILE_NAME
                    
                };



                var options_sass = {
                    SASS_OUT_PATH: SASS_OUT_PATH,
                    SASS_IN_PATH: SASS_IN_PATH,
                    SASS_OUT_FILE_NAME: SASS_OUT_FILE_NAME
                    
                };


                var options_body = {
                    VIEW_T_OUT_BODY: VIEW_T_OUT_BODY,
                    VIEW_T_IN_FRAG: VIEW_T_IN_FRAG
                    
                };

                   return (javaScript(options_script) && SCSS(options_sass) && BODY_HTML(options_body) );

 

      
  },
 get_function: function ()
 {
     return this.af_thankyou_mode ;
 }




};